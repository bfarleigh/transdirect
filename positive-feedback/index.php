<?
ini_set('session.cache_limiter', 'private');
session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Transdirect Positive Feedback</title>

<meta name="keywords" content="low volume freight door to door ebay" />
<meta name="description" content="Low volume and ebay freight" />
<meta name="classification" content="" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="en-au" />
<meta name="robots" content="index, follow " />
<link rel="stylesheet" href="/css/screen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/print.css" type="text/css" media="print" />
<link rel="stylesheet" href="/css/sIFR-screen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/sIFR-print.css" type="text/css" media="print" />
<link rel="stylesheet" href="/css/nav.css" type="text/css" media="screen" />
<link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="/inc/js/swfobject.js"></script>
<script type="text/javascript" src="/inc/js/sifr.js"></script>
<script type="text/javascript" src="/inc/js/mootools.v1.11.js"></script>
<script type="text/javascript" src="/inc/js/std-scripts.js"></script>
<script type="text/javascript" src="/inc/js/ss.js"></script>
<!--[if lt IE 7]><script language="javascript" type="text/javascript" src="/inc/js/sleight.js"></script><![endif]-->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5698511-1");
pageTracker._trackPageview();
</script>
<!-- search engine tracking script V.1.0 --> 
<script type="text/javascript">//<![CDATA[ 
var ns_data,ns_hp,ns_tz,ns_rf,ns_sr,ns_img,ns_pageName; 
ns_pageName= this.location; 
document.cookie='__support_check=1';ns_hp='http'; 
ns_rf=document.referrer;ns_sr=window.location.search; 
ns_tz=new Date();if(location.href.substr(0,6).toLowerCase() == 'http:') 
ns_hp='http';ns_data='&an='+escape(navigator.appName)+ 
'&sr='+escape(ns_sr)+'&ck='+document.cookie.length+ 
'&rf='+escape(ns_rf)+'&sl='+escape(navigator.systemLanguage)+ 
'&av='+escape(navigator.appVersion)+'&l='+escape(navigator.language)+ 
'&pf='+escape(navigator.platform)+'&pg='+escape(ns_pageName); 
ns_data=ns_data+'&cd='+screen.colorDepth+'&rs='+escape(screen.width+ ' x '+screen.height)+ 
'&tz='+ns_tz.getTimezoneOffset()+'&je='+ navigator.javaEnabled(); 
ns_img=new Image();ns_img.src=ns_hp+'://tracker.statgauge.com/statistics.aspx'+ 
'?v=1&s=213&acct=225183'+ns_data+'&tks='+ns_tz.getTime(); //]]> 
</script> 
<!-- End search engine tracking script -->
<script type="text/javascript">
window.onload = checkInsurance;
function checkInsurance(){
	if(document.qq.insurance.checked){
		document.qq.declared_value_field.style.display = 'block';
	}else{
		document.qq.declared_value_field.style.display = 'none';
		document.qq.declared_value.value = '';
	}
}
</script>
<style>
#contact a {
    width: 199px;
}
</style>
</head>

<body>
	<div id="header-outer">
  	<div id="header">
    	<h1><a href="/airfreight" title=""><span>Why Transdirect? | Low volume freight | door to door | ebay freight</span></a></h1>   
      <a href="http://www.carbonneutral.com.au" target="_blank"><img class="last" src="/img/carbon-neutral-logo.gif" alt="Carbon Neutral"  border="no"/></a>
      </div>
    
    <div id="top-nav-span">
      <div id="top-nav-wrap">
        <ul id="top-nav">
        
            <li id="home"><a href="/new" title=""><span>Home</span></a></li>
            <li id="how-it-works"><a href="/new/how-it-works/" title=""><span>How It Works?</span></a>
				<ul>
					<li class="road"><a href="/new/how-it-works/road-freight/">Road Freight </a></li>
					<li class="air"><a href="/new/how-it-works/air-freight/">Air Freight</a></li>
				</ul>
			</li>
            <li id="why-transdirect"><a class="current" href="/new/why-transdirect-couriers/" title=""><span>Why Transdirect Couriers?</span></a></li>
            <li id="insurance"><a href="/new/parcel-insurance/" title=""><span>Parcel Insurance</span></a>
				<ul>
					<li class="road"><a href="/new/how-it-works/road-freight/">Road Freight </a></li>
					<li class="air"><a href="/new/how-it-works/air-freight/">Air Freight</a></li>
				</ul>
			</li>
            <li id="contact"><a href="/new/contact/" title=""><span>Contact Us</span></a></li>
        </ul>
        
        <h2 id="quick-quote-tab"><span>Quick Quote</span></h2>
      </div>   
    </div>
  </div>
  
<div id="subpage-outer">

  	<div id="content">
    	
    	<div id="left">
		<style>
		.padFix p{
			border-bottom: 1px solid #4FA4F2;
			padding-bottom: 10px;
		}
		</style>
				
    	  <div class="padFix">
			<h2>Positive Feedback</h2><br />
  
			<h3>Read some feedback from our clients.</h3>
			<p>I just want to thank your team for the fabulous job you did in getting my parcel to me this morning.  I hoped it would come but didn't think it possible due to it being the last delivery before Santa comes and also the huge workload you have.  Again thanks and if I need to have something collected and/or delivered in the future I will certainly use your company.
			Happy Christmas to you all and keep safe on the roads.</p>

			<p>I just wanted to let you know the service I have received from your company over the past month has been fantastic, I am a very happy customer and would recommend your company to anyone.<br />
			Thanks for all your hard work this year, have a safe and merry christmas.</p>

			<p>Hi to the Transdirect Team.
			I have done two orders with your company and both times I have been extremely impressed in how quickly and efficiently the process was in getting the stock to my customers in NSW.<br />
			Thanks very much and look forward to moving more stock with Transdirect next year.
			Merry Xmas.</p>

			<p>Just a quick note to say thank you for your excellent service.
			I had 2 boxes collected from my home in Eight Mile Plains Qld. on Wednesday afternoon and my daughter in Canberra rang me at 11.30a.m. today, Friday, to say the parcels has arrived.  Considering the work-load for your company must be horrendous at this time of year, it is truly refreshing to have a commitment actually completed on time as promised by you.<br />
			Once again, Thank you and Merry Christmas</p>

			<p>My Husband & I would like to thank you for getting our only daughters Xmas presents to her in Sydney safe & sound - she is so excited
			The guy who picked  the box up from our house was friendly & nice
			Will use your service again</br />
			Thankyou</p>

			<p>Hi, just a note to say thank you so much for the quick delivery of my parcels from Gawler.  We spoke a couple of times on the phone before Christmas.  I couldn't believe how quickly it arrived (the day after pickup) in good condition.  Top service and will recommend - cheers</p>

			<p>Hello there,<br />
			Just a quick note to say your web page is very easy to use.
			So far it has been an easy and fast transaction, which is what we need in business.
			We look forward to dealing with you again in the new year.<br />
			Thank you and Happy New Year</p>

			<p>Your company collected a parcel for me in Sydney yesterday afternoon
			and it was on my doorstep in the Wyong area before midday today. That
			is terrific service, especially this close to Christmas. Well done
			and thank you. I will be recommending your company to others who may
			have a similar need</p>

			<p>Hi there,<br />
			Just a quick email to congratulate your company on a highly professional
			service. On time, with no fuss and delivered all intact.
			Well done guys you make some of the other shipping companies look like
			rank amateurs...</p>

			<p>TO: Nicci at Transdirect<br />
			Thank you so much Nicci. This is the first time I have ever had to have something couriered and to tell you the truth I was a bit nervous. I have to commend you on your service so far. I have received an answer to my emails promptly and this has instilled in me some confidence. Thank you again</p>

			<p>Just a quick hello and a big thank you for the people working the Brisbane to Melbourne service. I had a package picked up Wednesday afternoon and dropped to my door Saturday evening, I'm very impressed by the speedy service. Well done Transdirect.</p>

			<p>Good morning,<br />
			Thank you very much for your service. We could track the movement of the box we had transported from Brisbane to Canberra and have been very happy with the service.
			We will certainly recommend Transdirect to others as well as using the company again, ourselves.</p>

			<p>Hi,<br />
			I recently used your company for a snowboard i had shipped from
			Brisbane to Sydney, I just wanted to say thanks! The parcel arrived
			record time and was sitting at my back door when i got home, i also
			used the net to track the were abouts of the parcel and it was very
			helpful! Not only was it fast, efficient and arrived undamaged it was
			also the cheapest by far! I was quoted by some companies almost 5
			times the price for the same item!
			Great service guys! I will definitely use your company again and will
			let my friends know!</p>

			<p>Hi There!
			just wanted to let you know that I was very satisfied with the service I received from Transdirect when I arranged to have a parcel from South Australia delivered to me in Victoria.
			Your booking system is very efficient:;  I was able to track the progress of the parcel's transit;  pick up was prompt as was delivery well within the timeframe predicted by you.
			Thankyou for your excellent service.  I will highly recommend your company.</p>

			<p>Hi sales team,just a line to say I received my item all ok. Thank you for a great service and I look forward to dealing with you again in the near future.</p>

			<p>Dear Transdirect,<br />
			I just wanted to thank you for your assistance with moving an item for me from Melbourne to Brisbane.  The whole process was fantastic, easy and at the right price.  Your Web Site and booking form was very easy to use and your sales staff were superb with help online and on the phone with my questions.  My item arrived quickly and efficiently.<br />
			I would recomend you guys to anyone and everyone.<br />
			Keep up the good work!!!</p>

			<p>Dear Sir/Madam,<br />
				I am a new customer who used your service for the first time last week.
				You safely delivered a box (full of glass) all the way from Melbourne to
				Perth in just 3 days! I am amazed by your efficiency, your pricing and
				how easy it was to organise via your website.
				I will definitely be using your services for all my freight needs in the
				future.<br />
				Thank you & kind regards,</p>

			<p>Hi,<br />
			I thought I'd give you some feed back after using your services a few times this year. The folk at your pickup point (Ashmore) have always been helpful and friendly, while all our customers appear to be happy with your service on their end. It therefore appears that things are looking good - and thank you. I just wish we had more business to give you. <br />Anyway
			All the very best,</p>

			<p>Dear Transdirect,<br />
			I have never used a company before like yours that was so fuss-free, easy to get a quote-and extremely reasonable too...and delivered the parcel in record time!<br />
			Just wanted to let you know.</p>

			<p>Good morning,<br />
			I would like to thank your company in the great, prompt services.   From the booking to the collection and especially the quick deliver to Melbourne we are both happy customers.    
			Will be recommended your services and myself and the receiving customer will be keeping your details on record.<br />
			Thanks again.</p>

			<p>Since writing to you, I have just unwrapped the parcel, and everything was intact, as expected.So once again, superlative service.
			Clearly, I will be looking to Transdirect for future transport of parcels.
			Many thanks again.</p>

			<p>I just wanted to congratulate you all at Transdirect for your excellent service.
				The online booking/tracking is excellent and delivery is on time.
			I have already recommended Transdirect to others.<br />
			Thanks again,</p>

			<p>Thanks, the parcel arrived safely today, job well done, thank you.  We will definitely use your services again in the future, very reliable and professional.</p>

			<p>Dear Sir/Madam,<br />
			Just a quick bit of feedback.  I recently used Transdirect to deliver a parcel from Sydney to country Victoria and I am pleased to say that you guys did an excellent job in every aspect.   I will definitely use Transdirect again and I would recommend your courier company to anyone.<br />
			Thank you.</p>

			<p>To: Trans Direct<br />
			We got a quote and booked a package delivery from Brisbane to Canberra via your website.
			It is the first time we used your courier service and we were very pleased with the entire transaction. Your website is well set up and gave clear instructions that my 15 year old son followed. He got the quote, booked the parcel, taped the paper work onto the two packages, paid the fee and even tracked the parcel all the way to Canberra. He was also able to inform the person at the other end exactly when it was delivered. The parcel was also picked up from our home during the time frame indicated on the booking and delivered without any damage. It is not often that transactions run so smoothly, so I feel that you should be praised for your efforts.
			Often, when I recommend any positive changes that I feel would enhance a business, I receive a reply stating that people don't act on one persons point of view.
			In this instance, I do hope you will appreciate this positive email about your business (even if it just from one person!).<br />
			We will certainly recommend and use your services again.<br />
			Best wishes & thanks team.</p>

			<p>just wanted to say- fantastic service- you had my parcel picked up at lunchtime in brisbane and it arrived lunchtime the next day at agnes water (150km north of bundaberg)
			thanks very much - we will definitely be recommending you to all in town here- all the other couriers take at least a week!</p>

			<p>To the team at Transdirect,<br />
			Just want to drop a line to say how fantastic your service is. Booked on Tuesday, picked up Thursday afternoon (from QLD) and delivered by Friday afternoon (in NSW). No hassles, no annoying waiting times on the phone, very well priced and delivered on time. Thank you for the stellar effort and keep it up.</p>

			<p>Hi Guys,a note of thanks for your fantastic effort last week. We recently had you ship two crates up to Queensland from Tortech in NSW and you guys made the transfer so simple and efficient!<br />
			So... we just wanted to let you know.  Thanks.<br />
			You will remain our preferred Freighter and  we are already reccommending you.</p>

			<p>Good morning, I just had to send thanks to you for a job well done. I am very grateful to your company,
			for prompt delivery of my item, and for the excellent communication, before and during consignment.
			I feel compelled to recommend you to anybody that should enquire of me, a good interstate courier service.
			Your advice pre consignment, addressed all my initial questions.<br />
			With your internet tracking service and prompt delivery, I had no time for any concerns as to the whereabouts of my item</p>

			<p>I Just wanted to send a quick e-mail to say thank you for the recent delivery you did for me. I have moved and sent furniture across Australia many times. The recent cabinet I sent to my sister through you was the easiest process out of all. It arrived safely, the on-line process was simple and clear, the 'fine' print easy to find and the drivers at both ends were professional.<br />
			Thank you and I look forward to using your services in the future.</p>

			<p>Hi, I just want to thank you and your drivers for such good service and a great on-line system.</p>

			<p>Hi Transdirect<br />
			We used your service for AOE......<br />
			I just wanted to relay my thanks and the thanks of the receiving party.
			Just 7 or 8 days Perth to regional NSW door to door is an awesome service!
			Everyone is impressed and will spread the good word</p>

			<p>Hi - I would like to say that I was very impressed with your service of pickup and prompt delivery including the system for ordering and tracking.<br />
			Thank you.</p>

			<p>Hi ... We sent a box yesterday from 4017 to 2285 and it arrived overnight.
			We appreciate the great service.   Thanks to all concerned.  Also appreciated is the courtesy and cheerfulness of your pick-up driver who kindly put our trolley back over our fence which is extremely helpful to a 75yo.  Could you please pass on our thanks for a job well done.</p>

			<p>Good morning. Just a brief message to let you know my consignment arrived today. Perfect condition, and really great service throughout the whole process. Cannot be faulted. Kind regards.</p>

			<p>On two occasions I have sent parcels from Perth to Melbourne via Transdirect and have to say thank you.   The service is excellent and we will no doubt be using it again.</p>

			<p>I am a first time user of Transdirect and I just wanted to say that I am very impressed with your excellent
			Service and reasonable pricing.  Great work.  I will certainly use your services on intersstate freight requirements again<br />
			Thanks</p>

			<p>My item was picked up in Eltham Vic 1400hrs Monday13 Dec and delivered North Rocks 1100 hrs Tues 14Dec.
			Just a note to thank you for the GREAT service.<br />
			A far cry from the "old days" when goods Melb - Syd could take a week</p>

			<p>Hi everyone,<br />
			I just wanted to say thank you for such a quick and careful delivery of the cartons. I will be recommending you guys to anyone looking for this kind of service.</p>

			<p>Hi,<br />
			So far I've used you several times with phenomenal service and price.. I've told several people about you and I will be using you for our IT company which provides remote IT support to rural areas and in some cases, point-to-point hardware repairs, and I use TransDirect.</p>

			<p>Hi Transdirect team I just wish to say that your service with my machine was excellent and the drivers were professional and very polite and the office staff were helpful even when I stuffed up with the dates. I will be recommending your service to my colleges and again thanyou for  your very helpful user friendly service. Kind Regards<br />
			Dear .....,<br />
			Thank you very much. This has been excellent customer service!
			It's been a pleasure working with you!</p>

			<p>I wish to send congratulations to the Transdirect team for a fantastic service.  I will use your service again and and again and I have already put you up on facebook as a recommendation.  Job well done.<br />
			Thankyou so much.</p>

			<p>Hi Transdirect,<br />
			This is the second time I am using your services to transport items to my home.
			It is such a great service you provide where we can order through internet, receive labels and periodically check and see how things are progressing from pick up to delivery.
			The price is economical and the service is very efficient and fast.
			I hope to recommend to others.<br />
			Thanks again for the excellent service.</p>

			<p>Hi Transdirect,<br />
			Item arrived today. Please let you company know that I am very impressed with the ease of applying for the courier/the price and delivery. This is the first time I have used a courier service.
			I checked out quite a few courier companies online and yours was very easy to book and hassle free.<br />
			Will use in the future for sure.</p>

			<p>Hi! Transdirect,<br />
			Thank you for your excellent delivery Service of  my received consignment Number AOE......
			Goods arrived on time and your pre advice by email was very much appreciated..5 Stars ***** Regards,</p>

			<p>Just had to tell you - that this is the best pick-up service we have ever had !!!!!!!!  Well done, I'll be back</p>

			<p>Gidday, I sent a consignment of 14 cartons from Brisbane to Melbourne through your company recently, and just wanted to drop you a line and say thanks, everything about your service (particularly the price) was excellent, and I'll definately be back whenever I have anything to courier.<br />
			Thanks again, keep up the good work</p>

			<p>Thanks. I always use Transdirect. Your service is excellent and I always tells others to use your service.</p>

			<p>Hi..i just got an online quote and was extremely pleased as to the easy
			process. The price was reasonable.
			A friend works for "Couriers Please"...and although they are reasonably
			priced, i find the idea of buying book of 15 vouchers quite hard... I send
			things every now and then....
			I have been looking for a long time on a good courier Australia Wide.....
			I asked a fellow ebayer sender and she suggested you to me...
			Ill put in a good word and post your logo on my ebay selling items...<br />
			Thanks</p>

			<p>Hi, Just a quick note... My carton was picked up Sydney 17/08/10 pm and
			delivered to Grafton 18/08/10am. Very very impressive service and will most
			definately spread the word. Thank you for such a fast and excellent
			service. Cheers</p>

			<p>To whom it may concern,
			This was the first time i have used Transdirect.
			I wanted to let you know i am very happy with the service.
			Will be using your service from now on.
			Thankyou Very Much.</p>

			<p>To whom it may concern,<br />
			I would like to thank your company for such a professional service in the transport of my goods. I will highly recommend your company for transport now with anything that I may sell. I have tried various transport companies some good, some bad, some very bad e.g no contact numbers, only via email. I have found Transdirect the best by far, and will keep on using this company. Keep up the good work.</p>

			<p>To whom it may concern,<br />
			I want to say a big thank you re a parcel that my son sent up from NSW to Queensland. The parcel was picked up from NSW on the Friday and was in Queensland by Monday.  We were very impressed by the service you provide and highly recommend your service to others.<br />
			Thank you.</p>

			<p>Hi<br />
			Just thanking you for a fantastic service provided. i sent an item with you last week and it was in its destination within 2 days. it was such a pleasure and no hassels!!!!
			Will be using you in the future!</p>

			<p>Hey folks - than you very much for that outstanding service and QUICK delivery of my rims from MLB (Werribee) up here to QLD Nundah!
			That was a ripper job - and thanxs to that nice driver for his nice help and assistance unloading ...</p>

			<p>Hi
			I have just used TransDirect as a casual ebay user and wanted to advise how impressed I was with the ease of the booking & your speed of service, I will certainly be recommending your company.<br />
			Thanks again</p>

			<p>Hi there
			I just wanted to let you know that I used your services for the first time last week and I have to say not only am I impressed but so is my client. Especially seeing has her package arrived on a Saturday!<br />
			Brilliant service and I will continue to use you!</p>

			<p>To TransDirect<br />
			Thank you for the excellent service you provided with the pick up and delivery of my goods. Everything went according to instructions and I was able to track my consignment on-line. I am happy to recommend TransDirect to anyone in the future.</p>

			<p>Hey all,<br />
			After going through the repeated agony of sending parcels via Australia Post - and all the inefficiencies that this entails, I must say I am absolutely amazed at how EASY and EFFICIENT your service is! It's a dream!
			I thank Australia Post for refusing my parcel (it had a greater than 140cm circumference tsk tsk!) and the 20 minute queue - because I would never have found YOU GUYS!
			You were cheaper than Aust Post, fully insured my goods and picked up and delivered door-to-door and it was delivered to Brisbane the next day - and best of all, I could do it all in just one minute on the web.<br />
			If only you could do all our letters as well!!!. </p>

			<p>Hi<br />
			My name is ......... I just recently used your company
			and I am just leaving a short note to say I was very impressed with
			your prompt service and professionalism and I would recommend
			your services to anyone.<br />
			Many thanks</p>

			<p>Hi, We used Transdirect for the first time this week and I wanted to let you know what a positive experience it was.
			Transdirect provide excellent value for money. Your service was professional, efficient and offered useful and prompt advice.
			We would happily use Transdirect again if the occasion arises and certainly recommend to friends and business colleagues.</p>

			<p>Just wanted to say "thanks" for providing such a great service!<br />
			I found the online tracking really simple to use. the door to door service is superb and I have recommended your company to a number of other people.<br />
			Thank you</p>

			<p>Hi Team,<br />
			I just wish to forward on my thanks for providing excellent service at fantastic rates. I had arranged for a box weighing 17kg to be collected from Adelaide (computer & accessories) for delivery to my son's address in Darwin.
			As promised within the email, the box was collected on Mon 23 November and arrived yesterday in Darwin (28 November), following confirmation by my son. It only cost me a little over $37 and I was astounded at the cheap rates and reasonable delivery time.
			I will certainly be using your services again and have passed your details onto others who may need a freight service.
			Once again, many thanks for a top service!<br />
			Kindest Regards</p>


			<p>Hello, We want to congratulate you and your team on the excellent service that you provide. Our parcel was picked up from Mount Waverley, Victoria yesterday about 11.30am and my daughter in West Ryde NSW has just phoned at 10am to say that it has been delivered which is a fantastic achievement. We found that your website was extremely user-friendly and it contained all the information that a sender would need. Additionally, your charges are fair and reasonable. We will have no hesitation in using your service again when the need arises. It is a pleasure to send a congratulatory email and wish you and your very efficient company all the very best in the future. Thank you from a very grateful customer</p>


			<p>Hello there,<br />
				I've been a transdirect customer three times now, having items delivered to me from three different states, most recent of which were two packages which you delivered to me today.
				I just wanted to say thank-you for such a great service. Your website is so easy to use, your rates are so reasonable, constant tracking updates easy to acess, and your deliveries so fast (interstate overnight!). I was so pleased today when I saw the packages had already arived, I felt compelled to let you know!<br />
				Thank-you! Keep up the good work!</p>


				<p>Thank you once again for your fast and so relieable service, I always tell anyone I know about you. You are so reasonable we feel also. A Huge thanks</p>

				<p>I must let you know how pleased we are with your transport service.  The item we sent from Brisbane to Melbourne on Tuesday was delivered at the front door of the recipient's house this morning.  To call this sort of service "excellent" is to understate the value.<br />
				Thank you for providing such wonderful service.</p>


				<p>Hil,<br />
				My client received the delivery yesterday.
				Only took 3 days to delivery to Perth.<br />
				I just want to thank you for your prompt and professional service.</p>

				<p>Hi there<br />
				just wanted to say that the recent shipment was easily one of the smoothest deals I have done, Very competitive cost, rapid delivery interstate and goods well looked after which being a computer is obviously a concern.
				I have been using other freight companies regularly (Toll-Ipec mostly) and your service and rates are way out in front.<br />
				Thank you again and look forward to using TransDirect a lot more</p>


				<p>Hi Transdirect, Thanks for a speedy reply and detailed instructions for myself and my sender. A wonderful and prompt service and staff your company must have, and so user friendly made on line. I have never used courier like this but have no hesitation in recommending Transdirect to anyone. Many Thanks (A happy customer)</p>


				<p>I just wanted to say thanks for such great service and fast delivery.<br />
				My chair arrived this morning in perfect condition from Sydney to Melbourne and I was really impressed with your company.
				I did have a few fears intially as I have never booked anything online before or had anything transported from Interstate but I would certainly use your company again and would highly recommend it to all my family and friends with confidence.
				Thanks so much again<br />
				Regards</p>


				<p>Dear Trans Direct<br />
				I just wanted to say thank you for the terrific service I received from your company.  My light fittings arrived first thing this morning at the Gold Coast, after being picked up in Sydney late on Friday afternoon. They were all in perfect condition. I must also complement you on your price. It was so reasonable. As a regular EBay shopper, shopping interstate will now be real option for me when I link to Trans Direct.
				You will be pleased to know that I will be (and have already been) actively promoting Trans Direct to all my fellow Ebayers. I have saved your website to my favourites and will be back to use your services in the future.<br />
				Many thanks to the team at Trans Direct.</p>


				<p>Hi just a quick email to say thanks to your speedy delivery.
				I received the item Friday morning in good order. I will be recommending you to friends and family. Very happy with your service.</p>

				<p>Hi<br />
				Just a quick note to thank you so much for the professional and affordable service you provided us with this week.
				The goods arrived within 24 hours and in perfect condition.
				I will not hesitate to recommend you to our wide network of friends and family around the country.<br />
				Kind regards</p>

				<p>To Trans direct,<br />
				I would like to take the time to express my gratitude in receiving my package from South Australia to Victoria.
				Dealing with Trans direct has been a breeze and I will certainly be looking forward in dealing with you in the future again.<br />
				Thank You.</p>


				<p>I would like to send a message to the manager of your company to say how exellent and fast your service was on this item I had picked up from nsw, your price was great and it was delivered the next day to qld location. I buy and sell a lot of parts interstate and will use and recommend to buyers that they use your company. Thanks again and look forward to using your company again.</p>


				<p>Hi, parcel arrived today. Thank you so much. Please commend your people for job well done! Transdirect is briiliant!
				many thanks for everything.</p>

				<p>Hi,<br />
				Just wanted to drop a quick "Thankyou" for a parcel I had picked up from Melbourne and delivered to Shellharbour, NSW.  The parcel arrived very quickly and in good order and your rates were the cheapest i found anywhere, I obtained your services through the EBay site.  I would highly recommend your services to anyone but couldn't find where to leave positive feedback.<br />
				Thanks again,<br />
				Darlene</p>


				<p>Hi,<br />
				Just a quick note to say many thanks for the efficient handling of the above. The three cartons arrived yesterday. Will definitely recommend your service to others.<br />
				Thanks again</p>

				<p>Hi, I would like to say how impressed I was with your courier service. Not only was it half the price of the opposition but you have set up a process that is very easy to use and worked really well for me. I will be recommending you to my friends and colleagues. You deserve to do well.<br /> Regards,</p>


				<p>Hi,<br />
				Thank you very much for the excellent service. Merry Christmas.<br />
				Kind Regards</p>


				<p>HI THERE .<br />
				JUST A QUICK NOTE TO SAY --YOU GUYS ARE GREAT --
				I GET ALL MY EBAY CUSTOMERS TO USE YOU .<br />
				CHEERS --</p>


				<p>just wanted to say thankyou for your prompt delivery from melbourne to bega, we found the roofrack waiting for us when we got home and only 2 days. fantastic we will be using you again.<br />
				thanks again</p>

				<p>Hi, Just a quick thank you for your excellent service. The quilting frame was delivered today in excellent order. Many thanks</p>


				<p>Hi,<br />
				Our items arrived safe n sound,  thank you very much, its been a pleasure doing business with you.<br />
				Kind Regards</p>

				<p>Hi, This is just a thankyou, everything came through fine and you were very helpful, great to get someone who can get things done so smoothly and quickly... thanks again</p>

				<p>Dear Sir/Ms<br /><br />

				Just a note to say thank you very much for your excellent delivery service.
				For such a reasonable rate, I was a little concerned that things may
				not be straightforward. In fact, everything worked out perfectly and with great speed!
				I will definitely be using your services in the future and will certainly recommend your company to others.
				Thanks again<br />
				Kind regards</p>


				<p>Hi,<br />
				A quick thank you for the great service.  My 20 items all arrived in great time and in good condition.  Much appreciated!<br />
				Kind regards</p>

				<p>Brilliant. Thank you for your help and I will be sure to recommend this company for the ease.
				Kindest regards</p>

				<p>Hello Again, my guitar got delivered to my work address around 3.30 pm last Friday and it only got picked up from Melbourne the night before at 5.00 pm - GREAT STUFF!!!!<br />
				Thanks again, Brian.</p>


				<p>Good job, thankyou<br />
				My dad received the goods a couple of days ago (large Transit) and said the "guy was great, friendly, courteous & prompt"</p>


				<p>Good afternoon,<br />

				Just a quick note to thank you very much for the excellent service. The
				clock that I purchased was for pick up only, as the seller was concerned
				that it would not arrive without damage if sent by Australia Post or
				courier. Before I made my purchase I contacted the seller to ask if she
				would be prepared to package the old clock if I were to arrange
				transport & accept responsibility for the condition in which it arrived.
				Fortunately she agreed & I must say that she did an exceptional job of
				packaging it appropriately. That said, your staff obviously handled it
				with appropriate care for which I am very grateful.
				I have to also say that I found you, & your organisation, very
				professional to deal with & your organisation's communication was
				excellent. You were extremely co-operative when I needed to contact you
				& the constant feedback about arrival times etc was very helpful.<br />

				I am certainly not a power seller/buyer on EBAY & so wont require the
				services of a courier company frequently but if & when I do in the
				future I will look no further than Trans Direct.
				Could you please pass on my comments to your staff.<br />
				Many thanks & kind regards.</p>


				<p>Over the past month I have had occasion to use Trans Direct on-line service to organise the transport of e-Bay purchases from the eastern states of Australia to Western Australia.  I am writing to compliment you and your staff on the highly efficient service you operate, the speedy response to my communications, the ease of on-line bookings and the availability of on-line tracking service. I would recommend your service to others and will definitely continue to use Trans Direct.<br />
				Many, many thanks</p>

				<p>Hi,<br />
				Thank you for the AMAZING service! Sorry I wasn't in the unit when the driver arrived - as I said, I had just taken the boxes down to the garage to make it easier for the driver. I couldn't believe that he would arrive so quickly.
				Excellent service! Be assured that I will be passing on your company's name when I get the chance.<br />
				Cheers,</p>

				<p>Hi - I'd like to compliment you on your on line system.  It is a pleasure to use.  Everything works as it should. Your prices are also competitive.  Well done!<br />
				cheers</p>

				<p>Just wanted to thank you for a speedy courier service to Bundall, Qld. Both myself and the consignee are very impressed and appreciative.</p>


				<p>Hi, just a note to thank you for the excellent service provided by you and your company.
				I will recommend you to friends.<br />
				Regards</p>

				<p>Hello There,<br />
				Just a quick bouquet for your On Line Quoting Service - fantastic - compared to the opposition advertised on "Door to Door"  - +quotes and +rates on Google you leave them at the starting blocks - I have a consignment that I am looking at sending from the East you are cheaper and easier - well done.<br />
				Cheers and regards,</p>

				<p>Hi,<br />
				Once again thanks for the great service, 2 days door to door, all in good order. Will be using your company again for sure.<br />
				Many thanks</p>

				<p>I just wanted to acknowledge the excellent service in
				moving the above package. The package was collected from
				Cheltenham in Melbourne after noon on Tuesday, and delivered
				to me in South Brisbane shortly after 10 am on Thursday.
				I was able to follow progress on the tracking system, and will certainly
				be recommending your service to friends.<br />
				Regards,</p>

				<p>Dear Madam/sir<br />
				I have received my parcel from Airport today. Its in good condition.Thanks for your efficient service n' cooperation.<br />
				regards</p>

				<p>Hi, the guitar turned up at my place yesterday evening all in one piece so thanks for that. I will definitely be using your company again in the future and will recommend you to my friends too!<br />
				Thanks,</p>

    		
    	    <div class="float-fix"></div>
        </div>
       </div>
       <div id="right">
      	<div class="padFix">
      	 	  <form action="/new/quote/" method="get" class="std-form orange quick" name="qq">
		  <? if($_COOKIE['saved_sender1']) {?>
          	<fieldset class="sep inline">
            	<label for="member-number">Use this pickup address:</label>
				<select name="ss" style="font-size:11px;width:225px;" onChange="htmlData('/inc/ajax/get-from-postcode.php', 'id='+this.value)"><option value="">Choose...</option>
            	<? for($i=1; $i <= 10; $i++) { 
					$sender_array = explode('|', $_COOKIE["saved_sender$i"]);
					if($_COOKIE["saved_sender$i"]) echo "<option value='$i'>$sender_array[4] $sender_array[5] $sender_array[6] $sender_array[7]</option>";
				} ?>
				</select>
            </fieldset>
			<? }
			if($_COOKIE['saved_receiver1']) {?>
            <fieldset class="sep inline">
              <label for="casual-sender">Use this delivery address:</label>
			  <select name="sr" style="font-size:11px;width:225px;" onChange="htmlto('/inc/ajax/get-to-postcode.php', 'id='+this.value)"><option value="">Choose...</option>
            	<? for($i=1; $i <= 10; $i++) { 
				$receiver_array = explode('|', $_COOKIE["saved_receiver$i"]);
				if($_COOKIE["saved_receiver$i"]) echo "<option value='$i'>$receiver_array[4] $receiver_array[5] $receiver_array[6] $receiver_array[7]</option>";
				} ?>
				</select>
            </fieldset>
			<? } ?>
            <fieldset  class="sep">
              <fieldset class="small">
                <label for="postcode-from">*Postcode<br />of Origin</label>
                <div class="left orange"></div>
                <div id="from" style="display:inline;width: 87%;float: left;"><input type="text" id="from" name="from" /></div>
                <div class="right orange"></div>
              </fieldset>
              <div class="to-from-arrow"><div class="in"></div></div>
              <fieldset class="small">
                <label for="postcode-to">*Postcode of<br />Destination</label>
                <div class="left orange"></div>
               <div id="topc" style="display:inline;"> <input type="text" id="to" name="to" /></div>
                <div class="right orange"></div>
              </fieldset>
            </fieldset>
			       	  <a class="more-than-one" href="/airfreight/quote-more" title=""><span class="hide">Have more than 1 item?</span></a>
            <fieldset class="sep">
              <fieldset>
                <label for="description">*Packaging Description</label>
				<div class="left orange"></div>
                <select id="description" name="description[]">
                	<option value="Carton">Carton</option>
                	<option value="Heavy Carton">Heavy Carton</option>
                	<option value="Skid">Skid</option>
                	<option value="Pallet">Pallet</option>
                	<option value="Tube">Tube</option>
                	<option value="Crate">Crate</option>
                	<option value="Satchel/Bag">Satchel/Bag</option>
					<option value="Envelope">Envelope</option>
                	<option value="Other/Misc">Other/Misc</option>
                </select>
                <div class="right orange"></div>
				<span style="font-size:9px;"><i>All items MUST be packaged to use our service</i><br></span>
              </fieldset>
              <fieldset>
                <label for="weight">*Weight (kg)</label>
                <div class="left orange"></div>
                <input type="text" id="weight" name="weight[]" />
                <div class="right orange"></div>
              </fieldset>
              <label for="dim1">*Dimensions (L x W X H - cm)</label>
              <fieldset class="mini">
              	<div class="left orange"></div>
              	<input type="text" id="length" name="length[]" />
                <div class="right orange"></div>
              </fieldset>
              <div class="cross"><div class="inner"></div></div>
              <fieldset class="mini">
              	<div class="left orange"></div>
              	<input type="text" id="width" name="width[]" />
                <div class="right orange"></div>
              </fieldset>
              <div class="cross"><div class="inner"></div></div>
              <fieldset class="mini">
              	<div class="left orange"></div>
              	<input type="text" id="height" name="height[]" />
                <div class="right orange"></div>
              </fieldset>
            </fieldset>
            <fieldset class="inline quo">
            	<label for="sender-receiver">Are you:</label>
              <label class="radio" for="sender-receiver_0"><input type="radio" name="sender-receiver" value="sender" id="sender-receiver_0" /> Sender</label>
              <label class="radio" for="sender-receiver_1"><input type="radio" name="sender-receiver" value="receiver" id="sender-receiver_1" /> Receiver</label>
            </fieldset>
            <div class="float-fix"></div>
            <fieldset class="sep">
              <label for="service">Service type:</label>
		<div class="left orange"></div>
              <select name="service" id="service">
              	<option value="next">Next Flight</option>
              	<option value="overnight">Overnight</option>
              	<option value="2day">2 Day Economy</option>
              	<option value="3day">3 Day Off Peak</option>
              </select>
		<div class="right orange"></div>
            </fieldset>
            <div class="float-fix"></div>
            <div class="float-fix"></div>
            <fieldset class="inline quo">
              <label for="dangerous">Dangerous goods?</label>
              <label class="radio" for="dangerous_0"><input type="radio" name="dangerous" value="yes" id="dangerous_0" /> Yes</label>
              <label class="radio" for="dangerous_1"><input type="radio" name="dangerous" value="no" id="dangerous_1" /> No</label>
            </fieldset>
	    <fieldset class="inline quo">
              <label for="dangerous">Transit Warranty?</label>
              <label class="checkbox" for="insurance"><input type="checkbox" name="insurance" value="Y" id="insurance" onclick="checkInsurance()"/></label>
            </fieldset>
	    <fieldset id='declared_value_field' style="display: none;">
		<label for="declared_value">Declared Value of Item</label>
		<div class="left orange"></div>
	    	<input type="text" name="declared_value" width = "10" maxlength="4">
		<div class="right orange"></div>
	    </fieldset>
            <div class="float-fix"></div>
            <fieldset class="last">
            <input value="submit" type="image" src="/img/quote-button.gif" id="quote-button" /><br /><br />
            <div id="feature2">
			          <script type="text/javascript">
						// <![CDATA[
						
						var so = new SWFObject("/new/TD_Freight_Warranty.swf", "trans_small_flash", "220", "140", "7", "#FFFFFF");
						so.addParam("wmode", "transparent");
						so.write("feature2");
						
						// ]]>
					</script></div></fieldset>
            </form>
        </div>
      </div>
      <div class="float-fix"></div> 
    </div>
  </div>
  
  
  <div id="footer-outer">
  	<div id="footer">
    	<div class="left">
    		<p><strong>TransDirect</strong> - Post, send, transport, freight or courier your package or parcel from A to B - Ebay specialists!<br />
        <script type="text/javascript">generateEmailLink('info', 'transdirect.com.au');</script><br />
        ph. 1300 668 229</p>
      </div>
      <div class="right">
      	<p><a href="/airfreight/contact" title="">CONTACT</a> | <a href="/airfreight/terms/" title="">TERMS &amp; CONDITIONS</a></p>
        <p>&copy; 2008-2012 TransDirect - <a href="/airfreight/resources">Resources</a> <br>
			<a href="http://courier-brisbane.transdirect.com.au/">Brisbane</a> - <a href="http://courier-adelaide.transdirect.com.au/">Adelaide</a> - 
			<a href="http://courier-canberra.transdirect.com.au/">Canberra</a> - <a href="http://courier-darwin.transdirect.com.au/">Darwin</a> - 
			<a href="http://courier-hobart.transdirect.com.au/">Hobart</a> - <a href="http://courier-melbourne.transdirect.com.au/">Melbourne</a> - 
			<a href="http://courier-perth.transdirect.com.au/">Perth</a> - <a href="http://courier-sydney.transdirect.com.au/">Sydney</a>
		</p>
      </div>
      <div class="float-fix"></div>
    </div>
  </div>
</body>
</html>
