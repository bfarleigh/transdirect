<?php
set_time_limit(0);
ini_set('memory_limit', '160000000M');

require_once('simple_html_dom.php');

function getCokies($url,$postFields){

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
	preg_match('/^Set-Cookie: (.*?);/m', curl_exec($ch), $m);
	return $m[1];
}


?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Courier Australia, Interstate Couriers, Courier Service Australia, Sydney, Melbourne, Brisbane, Courier Delivery, Australian Courier Company, Perth, Adelaide, Hobart, Darwin, Canberra - Parcel Courier Service - Door to Door Anywhere in Australia!</title>
<meta name="keywords" content="Post, send , transport, freight, courier, package, parcel, ebay, shipping" />
<meta name="description" content="Transdirect door to door freight for everybody." />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="en-au" />
<meta name="robots" content="index, follow " />
<link rel="stylesheet" href="/css/screen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/print.css" type="text/css" media="print" />
<link rel="stylesheet" href="/css/sIFR-screen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/sIFR-print.css" type="text/css" media="print" />
<link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="/inc/js/swfobject.js"></script>
<script type="text/javascript" src="/inc/js/sifr.js"></script>
<script type="text/javascript" src="/inc/js/mootools.v1.11.js"></script>
<script type="text/javascript" src="/inc/js/std-scripts.js"></script>
<!--[if lt IE 7]><script language="javascript" type="text/javascript" src="/inc/js/sleight.js"></script><![endif]-->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5698511-1");
pageTracker._trackPageview();
</script>

<style type="text/css">
<!--
.style3 {font-size: 14px}
.style6 {font-size: 16px; color: #000099; }
.style8 {font-size: 14px; color: #FF9933; }
.style9 {font-size: 16px; color: #000000; }
.detailsTable
{
	width: 500px;
}

.detailsTable td
{
	vertical-align: top;
}

.detailsTable tr
{

}
.headerTable
{
	margin: 0px; 
	padding: 0px;  
	width: 100%;
}

.podImage
{
	width: 500px;
}
table
{
	font-size: 11px;
}

th
{
	background-color: #4F4F4F;
	font-size: 12px;
	font-weight: bold;
	text-align: center;
}

-->
</style>
<!-- search engine tracking script V.1.0 --> 
<script type="text/javascript">//<![CDATA[ 
var ns_data,ns_hp,ns_tz,ns_rf,ns_sr,ns_img,ns_pageName; 
ns_pageName= this.location; 
document.cookie='__support_check=1';ns_hp='http'; 
ns_rf=document.referrer;ns_sr=window.location.search; 
ns_tz=new Date();if(location.href.substr(0,6).toLowerCase() == 'http:') 
ns_hp='http';ns_data='&an='+escape(navigator.appName)+ 
'&sr='+escape(ns_sr)+'&ck='+document.cookie.length+ 
'&rf='+escape(ns_rf)+'&sl='+escape(navigator.systemLanguage)+ 
'&av='+escape(navigator.appVersion)+'&l='+escape(navigator.language)+ 
'&pf='+escape(navigator.platform)+'&pg='+escape(ns_pageName); 
ns_data=ns_data+'&cd='+screen.colorDepth+'&rs='+escape(screen.width+ ' x '+screen.height)+ 
'&tz='+ns_tz.getTimezoneOffset()+'&je='+ navigator.javaEnabled(); 
ns_img=new Image();ns_img.src=ns_hp+'://tracker.statgauge.com/statistics.aspx'+ 
'?v=1&s=213&acct=225183'+ns_data+'&tks='+ns_tz.getTime(); //]]> 
</script> 
<!-- End search engine tracking script -->  
</head>

<body>
	<div id="header-outer">
  	<div id="header">
    	<h1><a href="/" title=""><span>Post / send / transport / freight / courier your package or parcel from A to B - Ebay specialists!</span></a></h1>   
      <a class="airlink" href="/accountquery/" onclick="_gaq.push(['_trackEvent', 'Account Query', 'Button Click', '/accountquery/']);"></a>
	  </div>
    
    <div id="top-nav-span">
      <div id="top-nav-wrap">
        <ul id="top-nav">
        
            <li id="home"><a href="/" title=""><span>Home</span></a></li>
            <li id="how-it-works"><a class = "current" href="/how-it-works/" title=""><span>How It Works?</span></a></li>
            <li id="why-transdirect"><a href="/why-transdirect-couriers/" title=""><span>Why Transdirect Couriers?</span></a></li>
            <li id="insurance"><a href="/parcel-insurance/" title=""><span>Parcel Insurance</span></a></li>
            <li id="contact"><a href="/contact/" title=""><span>Contact Us</span></a></li>
        </ul>
      </div>   
    </div>
  </div>
  
<div id="subpage-outer" class="quote">

  	<div id="content">
    	
    	<div id="left">
				
    	  <div class="padFix">
<a name="top"></a>
<?
require_once('../calc/config.inc.php');
//echo '<pre>';print_r($_GET);exit;
if(!mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS))
	die('Could not connect to the database.');
if(!mysql_select_db(MYSQL_DB))
	die('Could not select the database.');
//$connote = $_GET['connote']; //'AOE148656';

/********************* T O L L **************************/

//Check if this is a Toll cannote.
if(strpos($_GET['connote'],"846827")===false){
	$connoteInTable = 'XXXXXXX';
}
else{
	$connoteInTable = substr(trim($_GET['connote']),6);
}

$ck_toll_query = mysql_query("SELECT `id`,`connote` FROM `bookings_toll` WHERE `id`='".mysql_real_escape_string(trim($_GET['connote']))."' OR `connote`='".mysql_real_escape_string($connoteInTable)."' LIMIT 1");

if(mysql_num_rows($ck_toll_query)){
	$tollOrderDetails = mysql_fetch_row($ck_toll_query);
	$connote = $tollOrderDetails[1];
	if(stristr($connote, '846827') === false) $connote = '846827'.$connote; // check for 846827 at front, add it if not there

	if($connote){
			$url ='https://online.toll.com.au/trackandtrace/traceConsignments.do';
			$postFields = 'consignments='.trim($connote);

			$sessionId = (getCokies($url,$postFields, $ckfile));

			
		//	$urlTocall =  "https://online.toll.com.au/trackandtrace/consignmentDetails.do;".$sessionId."?consignment=".trim($connote)."&recordCreatedBy=FIMS&groupId=";
		//	$html = str_get_html(file_get_contents($urlTocall));

			
			$opts = array('http' => array('header'=> 'Cookie: '.$sessionId."\r\n"));
			$context = stream_context_create($opts);

			$urlTocall =  "https://online.toll.com.au/trackandtrace/consignmentDetails.do?consignment=".trim($connote)."&recordCreatedBy=FIMS&groupId=";
			$html = str_get_html(file_get_contents($urlTocall,false,$context));

			foreach($html->find('div[class=errors]',0)->find('font') as $e){
				$error = $e->innertext;
			} 

			if($error){
				echo '<h2>Consignment Information: '.$connote.'</h2>';
				echo '<h3>Consignment not found. </h3>';
			}else{
		
						echo '<h2>Consignment Information: '.$connote.'</h2>';
					
						echo '<table class="detailsTable" cellspacing="5" cellpadding="10" id="dimensionsTable">';
						?>
						<tr>
						<th>Date</th>
						<th>Location</th>
						<th>Items</th>
						<th>Tracking Event</th>
						<th>Destination</th>
						</tr>
						<?php
						foreach($html->find('td[colspan=2] > table[width=100%]',3)->find('tr') as $tr){
							echo '<tr>';
							$cnt=0;
							foreach($tr->find('td') as $td){
								if($cnt==0){ $cnt++; continue;}
									echo '<td>';
									echo trim($td->plaintext);
									echo '</td>';
							}
							echo '</tr>';

						}
						echo '</table>';
			}
			
				$html->clear(); 
				unset($html);

				?>
	<a href="/onlinequery/?consignment=<?=$connote?>"><button style="padding:5px;margin-top:10px;">OPEN AN ONLINE QUERY - CLICK HERE</button></a>

	<?
			}
		
}
	
/********************* T O L L **************************/
else{

$inTabCN = substr(trim($_GET['connote']),3);
// check in Allied table it its with ID and get cannote number.
$ck_allied_id = mysql_query("SELECT `connote`,`receiver_id` FROM `bookings` WHERE `id`='".mysql_real_escape_string(trim($_GET['connote']))."' OR `connote`='".mysql_real_escape_string($inTabCN)."' LIMIT 1");
if(mysql_num_rows($ck_allied_id)){
			$resAllied  = mysql_fetch_row($ck_allied_id);
			$connote = $resAllied[0];
 			
			$despc_q = mysql_query("SELECT `postcode` FROM `addresses` WHERE `id`='".mysql_real_escape_string($resAllied[1])."' LIMIT 1");

			if(mysql_num_rows($despc_q)){
				$despc_res  = mysql_fetch_row($despc_q);
				$destpc = $despc_res[0];
			}

if(stristr($connote, 'aoe') === false) $connote = 'aoe'.$connote; // check for aoe at front, add it if not there
$connote = strtoupper($connote);
if(stristr($connote, '-0')) { // they put the -00x on the end, trim it...
	$trimpos = strpos($connote, '-0');
	$connote = substr($connote, 0, $trimpos);
}

$destpc = str_pad($destpc, 4, "0", STR_PAD_LEFT); // added on 2/11/2013

$data = file_get_contents('http://neptune.alliedexpress.com.au/iTrack/search.do?sig=0C5E96954FB23091E0E4A795C8063C67&ono=&tno='.$connote.'&dpc='.$destpc.'&searchType=con');
$startpos = strpos($data, '<!-- Start Page Content -->');
$endpos = strpos($data, '<!-- End Page Content -->');
$data = substr($data, $startpos, ($endpos - $startpos));
$data = str_replace('h1', 'h2', $data);
$data = str_replace('cellspacing="1"', 'cellspacing="5"', $data);
$data = str_replace('cellpadding="3"', 'cellpadding="10"', $data);
$data = str_replace('caption', 'h3', $data);
$data = str_replace("src='/iTrack/", "src='http://neptune.alliedexpress.com.au/iTrack/", $data);
$data = str_replace('<td class="column1">Weight</td>', '<td class="column1">Charged Weight</td>', $data);

// Remove all the job info from the top, especially the damn scheduled delivery date.....
if(stristr($data, '<fieldset id="jobDetails">')) { // exists
	// Work out where the scheduled delivery info starts and ends...
	$delivery_startpos = strpos($data, '<fieldset id="jobDetails">');
	$delivery_endpos = (strpos($data, '</fieldset>', $delivery_startpos) + 11); // the +11 is so the fieldset is removed too
	// Chop out the scheduled delivery date, as it is bullsh
	$result = substr($data, 0, $delivery_startpos);
	$result.= substr($data, $delivery_endpos);
} else { // nope delivery date does not exist, leave alone
	$result = $data;
}

// chop out bazza's error message if it exists
if(stristr($result, 'Urgent Notice'))
{
	$bazza_startpos = strpos($result, '<fieldset id="jobDetails" style="color:#000000; background-color:#FFFFFF; font-size: 14px; ">');
	$bazza_endpos = (strpos($result, '</fieldset>', $bazza_startpos) + 11); // the +11 is so the fieldset is removed too
	$newresult = substr($result, 0, $bazza_startpos);
	$newresult.= substr($result, $bazza_endpos);
} else {
	$newresult = $result;
}// enough of bazza's crapola

// spit it out...
echo $newresult;
?>

<p>&nbsp;</p>
<h3>Legend</h3>
<table>
<tr><td width="100px"><b><u>SCAN DATE</u></b></td><td>The date and time of current location, and status.</td></tr>
<tr><td colspan="2">&nbsp;</td></tr>

<tr><td><b><u>LOCATION</u></b></td><td>&nbsp;</td></tr>
<tr><td>MEL</td><td>The item is currently in Melbourne</td></tr>
<tr><td>SYD</td><td>The item is currently in Sydney</td></tr>
<tr><td>etc</td><td>etc</td></tr>
<tr><td colspan="2">&nbsp;</td></tr>

<tr><td><b><u>STATUS</u></b></td><td>&nbsp;</td></tr>
<tr><td>AMIN</td><td>The item scanned into the depot in the morning</td></tr>
<tr><td>PMIN</td><td>The item scanned into the depot in the afternoon</td></tr>
<tr><td>123OUT</td><td>The item is currently being delivered by driver 123</td></tr>
<tr><td>DEL</td><td>The item has been delivered</td></tr>
<tr><td>PODIN</td><td>The Proof of delivery is now available</td></tr>
<tr><td>MEL</td><td>The item is on the way to Melbourne</td></tr>
<tr><td>SYD</td><td>The item is on the way to Sydney</td></tr>
<tr><td>ADL</td><td>The item is on the way to Adelaide</td></tr>
<tr><td>PER</td><td>The item is on the way to Perth</td></tr>
<tr><td>ROADIN</td><td>Main linehaul truck has arrived in depot</td></tr>
<tr><td>CL</td><td>Delivery was attempted, and a card was left</td></tr>
<tr><td>etc</td><td>etc</td></tr>
</table>
<?php 
}
else{
echo '<h2>Consignment Information: '.$_GET['connote'].'</h2>';
				echo '<h3>Consignment not found. </h3>';

}
?>
<a href="/onlinequery/?consignment=<?=$connote?>"><button style="padding:5px;margin-top:10px;">OPEN AN ONLINE QUERY - CLICK HERE</button></a>
<?
} ?>

    	    <div class="float-fix"></div>
        </div>
       </div>
       <div id="right">
      	<div class="padFix">
        	
					  <h2 style="color:FF6D0C;">Translinks</h2>
		  <ul>
		  <li><a href="/faq" title="">Frequently Asked Questions</a></li>
           	  <li><a href="./#top" title="">How it Works</a></li>
           	 <? // <li><a href="./#compare" title="">Compare our Rates</a></li> ?>
            <li><a href="./#requirements" title="">Packing &amp; Sending Requirements</a></li>
            <li><a href="./#measuring" title="">Measuring Info</a></li>
            <li><a href="./#tracking" title="">Tracking Info</a></li>
            <li><a href="./#times" title="">Transit Times</a></li>
            <li><a href="./#regular" title="">Regular Sender?</a></li>
                      </ul>
        </div>
      </div>
      <div class="float-fix"></div> 
    </div>
  </div>
  
  
  <div id="footer-outer">
  	<div id="footer">
    	<div class="left">
    		<p><strong>TransDirect</strong> - Post, send, transport, freight or courier your package or parcel from A to B - Ebay specialists!<br />
        <script type="text/javascript">generateEmailLink('info', 'transdirect.com.au');</script><br />
        ph. 1300 668 229</p>
      </div>
      <div class="right">
      	<p><a href="/contact" title="">CONTACT</a> | <a href="/terms/" title="">TERMS &amp; CONDITIONS</a></p>
        <p>&copy; 2008 TransDirect - <a href="/resources">Resources</a> <br>
			<a href="http://courier-brisbane.transdirect.com.au/">Brisbane</a> - <a href="http://courier-adelaide.transdirect.com.au/">Adelaide</a> - 
			<a href="http://courier-canberra.transdirect.com.au/">Canberra</a> - <a href="http://courier-darwin.transdirect.com.au/">Darwin</a> - 
			<a href="http://courier-hobart.transdirect.com.au/">Hobart</a> - <a href="http://courier-melbourne.transdirect.com.au/">Melbourne</a> - 
			<a href="http://courier-perth.transdirect.com.au/">Perth</a> - <a href="http://courier-sydney.transdirect.com.au/">Sydney</a>
		</p>
      </div>
      <div class="float-fix"></div>
    </div>
  </div>
</body>
</html>
