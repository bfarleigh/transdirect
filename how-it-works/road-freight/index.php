<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>How Transdirect Works - Road Freight</title>

<meta name="keywords" content="Post, send , transport, freight, courier, package, parcel, ebay, shipping" />
<meta name="description" content="Transdirect door to door freight for everybody." />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="en-au" />
<meta name="robots" content="index, follow " />
<link rel="stylesheet" href="/css/screen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/print.css" type="text/css" media="print" />
<link rel="stylesheet" href="/css/sIFR-screen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/sIFR-print.css" type="text/css" media="print" />
<link rel="stylesheet" href="/css/nav.css" type="text/css" media="screen" />

<link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="/inc/js/swfobject.js"></script>
<script type="text/javascript" src="/inc/js/sifr.js"></script>
<script type="text/javascript" src="/inc/js/mootools.v1.11.js"></script>
<script type="text/javascript" src="/inc/js/std-scripts.js"></script>
<!--[if lt IE 7]><script language="javascript" type="text/javascript" src="/inc/js/sleight.js"></script><![endif]-->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-5698511-1']);
  _gaq.push(['_setDomainName', 'transdirect.com.au']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<style type="text/css">
<!--
.style3 {font-size: 14px}
.style6 {font-size: 16px; color: #000099; }
.style8 {font-size: 14px; color: #FF9933; }
.style9 {font-size: 16px; color: #000000; }
-->
</style>
<!-- search engine tracking script V.1.0 --> 
<script type="text/javascript">//<![CDATA[ 
var ns_data,ns_hp,ns_tz,ns_rf,ns_sr,ns_img,ns_pageName; 
ns_pageName= this.location; 
document.cookie='__support_check=1';ns_hp='http'; 
ns_rf=document.referrer;ns_sr=window.location.search; 
ns_tz=new Date();if(location.href.substr(0,6).toLowerCase() == 'http:') 
ns_hp='http';ns_data='&an='+escape(navigator.appName)+ 
'&sr='+escape(ns_sr)+'&ck='+document.cookie.length+ 
'&rf='+escape(ns_rf)+'&sl='+escape(navigator.systemLanguage)+ 
'&av='+escape(navigator.appVersion)+'&l='+escape(navigator.language)+ 
'&pf='+escape(navigator.platform)+'&pg='+escape(ns_pageName); 
ns_data=ns_data+'&cd='+screen.colorDepth+'&rs='+escape(screen.width+ ' x '+screen.height)+ 
'&tz='+ns_tz.getTimezoneOffset()+'&je='+ navigator.javaEnabled(); 
ns_img=new Image();ns_img.src=ns_hp+'://tracker.statgauge.com/statistics.aspx'+ 
'?v=1&s=213&acct=225183'+ns_data+'&tks='+ns_tz.getTime(); //]]> 
</script> 
<!-- End search engine tracking script -->  
</head>

<body>
	<div id="header-outer">
  	<div id="header">
    	<h1><a href="/" title=""><span>Post / send / transport / freight / courier your package or parcel from A to B - Ebay specialists!</span></a></h1>   
       <a class="airlink" href="/accountquery/" onclick="_gaq.push(['_trackEvent', 'Account Query', 'Button Click', '/accountquery/']);"></a>
	  </div>
    
    <div id="top-nav-span">
      <div id="top-nav-wrap">
        <ul id="top-nav">
            <li id="home"><a href="/" title=""><span>Home</span></a></li>
            <li id="how-it-works"><a class = "current" href="/how-it-works/" title=""><span>How It Works?</span></a>
				<ul>
					<li class="road"><a href="/how-it-works/road-freight/">Road Freight </a></li>
					<li class="air"><a href="/how-it-works/air-freight/">Air Freight</a></li>
				</ul>
			</li>
            <li id="why-transdirect"><a href="/why-transdirect-couriers/" title=""><span>Why Transdirect Couriers?</span></a></li>
            <li id="insurance"><a href="/parcel-insurance/" title=""><span>Parcel Insurance</span></a>
				<ul>
					<li class="road"><a href="/parcel-insurance/road-freight/">Road Freight </a></li>
					<li class="air"><a href="/parcel-insurance/air-freight/">Air Freight</a></li>
				</ul>
			</li>
            <li id="contact"><a href="/contact/" title=""><span>Contact Us</span></a></li>
        </ul>
      </div>   
    </div>
  </div>
  
<div id="subpage-outer" class="quote">

  	<div id="content">
    	<div id="left">
				
    	  <div class="padFix">
<a name="top"></a>
<h3>Road freight - Frequently asked questions</h3>
<p class="style3">Please visit our <a href="/faq/road-freight/">frequently asked questions</a> page to find answers to common questions users have about our service.  After reading our <a href="/faq/road-freight/">frequently asked questions</a> and this 'how it works' page, you should be familiar with our service & ready to book your consignment!</p>
<p>&nbsp;</p>

<a name="howitworks"></a>
<h3>How it works  - <a href="#top">^</a>  </h3></h3>
<ul style="font-size:14px;">
		<li> <span class="style9">Step 1:</span> Get a quick quote. It's so simple!</li>
		<li> <span class="style9">Step 2:</span> Pay and confirm your details through our secure payment facility.</li>
		<li> <span class="style9">Step 3:</span> You will receive an email with an attachment including a receipt and a consignment note.</li>
		<li> <span class="style9">Step 4:</span> Print out the consignment note and securely stick it to the carton.<br />
		  <br />
		  <span style="text-align:center;"><a href="../img/connote_l.gif" target="_blank"><img src="/img/connote_s.gif" border="no" /></a></span><br />
		  <ul><li>   Cut the consignment note along the dotted line in between the ‘SENDERS COPY’ and the ‘POD COPY’. Keep the now detached ‘Senders copy’ for your record of the delivery details.</li>
			<li>Fold the Consignment note along the dotted line between the ‘CARTON LABEL’ and the ‘POD COPY.</li>
			<li> Using clear sticky tape, attach the ‘CARTON LABEL’ part of the consignment note so that the ‘POD COPY’ part is loose and able to be torn off at the destination and kept by the driver.</li>
		  <li>Fold the ‘POD COPY’ half of the consignment note over the attached ‘CARTON LABEL’ part, and that’s it, your parcel is ready for pickup. </li></ul>
 Make sure you write the delivery address and the consignment number on the carton, just in case the label is accidentally removed. You must have the label secured on the carton for the pickup to be taken.</li>

		<li><span class="style9">Step 5:</span> One of our chosen delivery companies will come and pick up the carton on the day and time specified.</li>
		  <li><span class="style9">Step 6:</span> You can track and trace your delivery status on our web site, using your consignment number.</li>
		  <li><span class="style9">Step 7:</span> Your consignment will be delivered between the hours of 9am - 5pm.</li>
          <p>&nbsp;</p>
</ul>
<?
/*
<a name="compare"></a><h3>Compare our Rates</h3>
<table border="1">
  <tr><th align="center"><span class="style3">Company Name</span></th><th colspan="4" align="center"><span class="style3">Route &amp; Weight</span></th><th colspan="3" align="center"><span class="style3">Services offered</span></th></tr>
<tr><td bgcolor="#000066"><span class="style3">&nbsp;</span></td><td align="center" bgcolor="#000066"><span class="style8">Gold Coast - Melbourne 15kgs</span></td>
<td align="center" bgcolor="#000066"><span class="style8">Gold Coast - Perth 15kgs</span></td>
<td align="center" bgcolor="#000066"><span class="style8">Perth - Melbourne 30kgs</span></td>
<td align="center" bgcolor="#000066"><span class="style8">Sydney - Melbourne 30kgs</span></td>
<td bgcolor="#000066"><span class="style8">Door to Door</span></td><td bgcolor="#000066"><span class="style8">Warranty Included</span></td><td bgcolor="#000066"><span class="style8">Carbon Offset Included</span></td></tr>
<tr>
  <td bgcolor="#000066"><span class="style8">Australia Post *</span></td><td align="center">$25.05 </td>    
<td align="center">$56.05 </td>
<td align="center">Overweight limit</td>
<td align="center">Overweight limit</td><td align="center">NO</td>
<td align="center">NO</td>
<td align="center">NO</td> 
</tr>
<tr>
  <td bgcolor="#000066"><span class="style8">Pack and Send * </span></td><td align="center">$80.30 </td>    <td align="center">$142.00 </td>
<td align="center">$125.00 </td>
<td align="center">$167.00 </td><td align="center">YES</td>
<td align="center">NO</td>
<td align="center">NO</td> 
</tr>
<tr>
  <td bgcolor="#000066"><span class="style8">TNT * </span></td><td align="center">$81.84 </td>    
<td align="center">$113.32 </td>
<td align="center">$178.27 </td>
<td align="center">$115.95 </td><td align="center">YES</td>
<td align="center">NO</td>
<td align="center">NO</td> 
</tr>
<tr>
  <td bgcolor="#000066"><span class="style8">Airroad Direct * </span></td><td align="center">$88.72 </td>    
<td align="center">$109.78 </td>
<td align="center">$104.72</td>
<td align="center">$104.72 </td><td align="center">YES</td>
<td align="center">Yes, up to $1,000</td>
<td align="center">NO</td> 
</tr>
<tr><td bgcolor="#FF9900"><span class="style6">Transdirect</span></td>
<td align="center" bgcolor="#FF9900"><span class="style6">$28 </span></td>    
<td align="center" bgcolor="#FF9900"><span class="style6">$47.80 </span></td>
<td align="center" bgcolor="#FF9900"><span class="style6">$54.36</span></td>
<td align="center" bgcolor="#FF9900"><span class="style6">$31.05 </span></td>
<td align="center" bgcolor="#FF9900"><span class="style6">YES</span></td>
<td align="center" bgcolor="#FF9900"><span class="style6">Yes, up to $12,500</span></td>
<td align="center" bgcolor="#FF9900"><span class="style6">YES</span></td> 
</tr></table>
<p style="font-size:14px">* All prices correct as of 1st August 2008<br />
&nbsp; All prices include GST and Fuel Levy<br />
&nbsp; All prices are based on non-account holding customers<br />
*/ ?>
<a name="requirements"></a>
<h3>Packing &amp; Sending Requirements  - <a href="#top">^</a>  </h3>
<ul style="font-size:14px;">
		<li> Don't make the carton too full or too heavy. If the contents of the carton can be split and it weighs over 30kg, put it in two cartons.</li>
		<li> Make sure your box/carton is structurally sturdy.</li>
		<li> Don't over pack your carton.</li>
		<li> Make sure your goods have cushioning around it so it reduces the chances of getting damaged in transit.</li>

		<li> Make sure the cartons are well sealed with wide robust tape. Secure the carton to eliminate it from opening whilst in transit.</li>
		<li> If your goods are fragile, let us know by clearly writing on the carton <strong>FRAGILE!</strong></li>
		<li> We do not send:<br />
		  - dangerous goods
		  <br />

		   - any unpackaged items<br />
	      - items that exceed 4 metres in length</li>
		<li> Make sure you are aware of the <a href="/terms/" target="_blank">terms and conditions.</a></li>
		<li> Once you have booked and paid for the pickup you will receive an attachment of your receipt and consignment note via email. You need to print out this consignment note and stick it on to your carton.<br />
	    </li>

		<li> Make sure your carton has no other address written on it, or any old delivery labels.</li>
		  <li> Write on the carton, the delivery address and the consignment number.</li>
          <li>All items must be packed  into a carton/box or covered in either bubblewrap or plastic depending on the  nature of the item. If damage can easily occur due to the packaging used, you  may void the transit warranty.</li>
		  <li>Suitcases and travel cases can be sent as they are. They do need to be securely fastened.</li>
		  <li>Large unpackaged furniture  items will not be accepted. These types of items would be best moved with a  specialised furniture removalist. You can contact our Customer service on 1300  668 229 if you need to discuss your delivery. </li>
		  <!--
		  <li>Items that exceed 60kgs must be packed on a skid/pallet or crate, or be packaged in a way that can be moved easily and safely by forklift. You must have a forklift or other lifting method available at both the sending & receiving addresses If you do not have the appropriate lifting equipment, you must select the Hydraulic Tail lift option for that location.</li>
		<li>If your packaged item is between 35 -60 kg's and you do not have a pallet, you must ensure there is sufficient assistance to load and/or unload the vehicle. Failure to have the assistance required will result in either a Futile pickup or Re-delivery fee.</li>
		-->
		<li>Items that exceed 40kgs must be packed on a skid/pallet or crate, or be packaged in a way that can be moved easily and safely by forklift. You must have a forklift or other lifting method available at both the sending & receiving addresses. If you do not have the appropriate lifting equipment, you must select the Hydraulic Tail lift option for that location.</li>
		<li>If you are sending a pallet, you need to ensure the item/s will remain stable during transit. If your item is of obscure shape, it may need to be packaged into a crate for our Transit Warranty to apply. All strapping used must be placed away from the areas used by forklift tongues during loading and unloading.</li>
</ul>
<p>&nbsp;</p>
<a name="measuring"></a>
<h3>Measuring the size of the carton -  <a href="#top">^</a> </h3>
<p class="style3">Please review the image below showing how to correctly measure the size of your carton(s):</p>
<p><img src="/img/measure.jpg" /></p>
<a name="tracking"></a>
<h3>Tracking your consignment - <a href="#top">^</a> </h3>
<p style="font-size:14px;">If you need to track your consignment at any time, just fill in the details below and press 'track and trace'. You will need to use the AOE number located under the barcode on the consigment note. </p>
<form name="frmQuickSearch" method="get" action="../track.php" target="quickSearch" style="margin-bottom: 0px;">
                      <table id="startTable">
                          <tr>
                              <td>Consignment/Reference:</td>

                              <td>
                                  <input type="text" name="connote" value="" display-name="consignment" initial-focus="on" required="true" regexp="^.{7,}$" regexp-message="Please enter a consignment greater than 6 characters" />&nbsp;                              </td>
                          </tr>
                          <tr>
                              <td>Destination Postcode:</td>
                              <td>
                                  <input type="text" name="dpc" size="4" value="" display-name="destination postcode" required="true" regexp="^\d{4}$" regexp-message="Please enter a valid 4 digit postcode" />&nbsp;                              </td>
                          </tr>
                          <tr>
                              <td>Search Type:</td>
                              <td>
                                  <input type="radio" name="searchType" value="con" checked />&nbsp; Consignment
                                  <input type="radio" name="searchType" value="ref" />&nbsp; Reference                              </td>
                          </tr>
                          <tr>
                              <td colspan="2" align="center">
                                  <input type="submit" name="submit" value="&nbsp;Track&nbsp;">                            </td>
                          </tr>
                      </table>
            </form>
<a name="times"></a>
<h3>Transit Times - <a href="#top">^</a></h3>
<p style="font-size:14px">If you wish to estimate the transit time of your delivery, please fill in the FROM and TO postcodes below:</p>
<div style="font-size: 14px; padding-top: 10px;">
                <table>
                <form action="index.php#times" method="post">
                   <tr>
                   <td>From Postcode:</td><td><input type="text" name="from"></td>
                   </tr>
                   <tr>
                   <td>To Postcode:</td><td><input type="text" name="to"></td>
                   </tr>
                   <tr>
                   <td></td><td><input type="submit" name="submit" value="Estimate Delivery Time"></td>
                   </tr>
                </table><?php
if(isset($_POST['submit']))
{
	require_once('../../calc_road/TransDirectRoad.php');
	$TransDirect = new TransDirectRoad;
	$time = $TransDirect->quoteTime($_POST['from'], $_POST['to']);
	if($time)
		echo 'The estimated delivery time is ' . $time . ' business days.<br><br>';
	else
		echo 'The delivery time could not be estimated.<br><br>';
}
?>
</div>
<p style="font-size:14px">Please ensure that the recipient of the delivery is available on or about the delivery date</p>
<br />
			
			<a name="regular"></a>
			<h3>Are you a regular sender or receiver? - <a href="#top">^</a> </h3>
			<p class="style3">Although we have structured our business to offer cheap rates across the board, we do offer clients that regularly send with transdirect extra savings by becoming a member. <br><Br>
If you send over $150 a week on your current freight requirements we will offer you up to 5% discount.<br><Br>
 If you send over $350 per week we will offer you up to 10% discount. <br><Br>
If you send over $700 per week we will offer you up to 15% discount.<br><Br>
 This will then enable you to have a Transdirect membership number, that will automatically apply the discounts when you book your jobs. <br><Br>
We also have a range of account options available to make the booking process easier and more efficient for your dispatch/wharehouse staff.

		    <p><span class="style3">To find out further details and whether you qualify, please <a href="/contact">contact us.</a></span></p>
            <h3>Questions?</h3>
            <p><span class="style3">If you have any  questions you can contact us at via the methods listed on our <a href="/contact">contact page</a></span></p>
    	    <div class="float-fix"></div>
        </div>
       </div>
       <div id="right">
      	<div class="padFix">
        	
					  <h2 style="color:FF6D0C;">Translinks</h2>
		  <ul>
			<li><a href="/faq/road-freight/" title="">Frequently Asked Questions</a></li>
           	  <li><a href="./#howitworks" title="">How it Works</a></li>
           	 <? // <li><a href="./#compare" title="">Compare our Rates</a></li> ?>
            <li><a href="./#requirements" title="">Packing &amp; Sending Requirements</a></li>
            <li><a href="./#measuring" title="">Measuring Info</a></li>
            <li><a href="./#tracking" title="">Tracking Info</a></li>
            <li><a href="./#times" title="">Transit Times</a></li>
            <li><a href="./#regular" title="">Regular Sender?</a></li>
                      </ul>
        </div>
      </div>
      <div class="float-fix"></div> 
    </div>
  </div>
  
  
  <div id="footer-outer">
  	<div id="footer">
    	<div class="left">
    		<p><strong>TransDirect</strong> - Parcel Courier Service - Door to Door Anywhere in Australia!<br />
        <script type="text/javascript">generateEmailLink('info', 'transdirect.com.au');</script><br />

        ph. 1300 668 229<br />
		<a href="http://www.djc.com.au/services/#myob-system-integration" target="_blank">MYOB Accounting System &amp; Web Integration by DJC</a></p>
      </div>
      <div class="right">
      	<p><a href="/contact" title="">CONTACT</a> | <a href="/terms/" title="">TERMS &amp; CONDITIONS</a></p>
        <p>&copy; 2008-2012 TransDirect - <a href="/resources">Resources</a> <br>
			<a href="http://courier-brisbane.transdirect.com.au/">Brisbane</a> - <a href="http://courier-adelaide.transdirect.com.au/">Adelaide</a> - 
			<a href="http://courier-canberra.transdirect.com.au/">Canberra</a> - <a href="http://courier-darwin.transdirect.com.au/">Darwin</a> - 
			<a href="http://courier-hobart.transdirect.com.au/">Hobart</a> - <a href="http://courier-melbourne.transdirect.com.au/">Melbourne</a> - 
			<a href="http://courier-perth.transdirect.com.au/">Perth</a> - <a href="http://courier-sydney.transdirect.com.au/">Sydney</a>
		</p>
      </div>
      <div class="float-fix"></div>
    </div>
  </div>
</body>
</html>
