<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>How Transdirect Works - Air Freight</title>

<meta name="keywords" content="Post, send , transport, freight, courier, package, parcel, ebay, shipping" />
<meta name="description" content="Transdirect door to door freight for everybody." />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="en-au" />
<meta name="robots" content="index, follow " />
<link rel="stylesheet" href="/css/screen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/print.css" type="text/css" media="print" />
<link rel="stylesheet" href="/css/sIFR-screen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/sIFR-print.css" type="text/css" media="print" />
<link rel="stylesheet" href="/css/nav.css" type="text/css" media="screen" />
<link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="/inc/js/swfobject.js"></script>
<script type="text/javascript" src="/inc/js/sifr.js"></script>
<script type="text/javascript" src="/inc/js/mootools.v1.11.js"></script>
<script type="text/javascript" src="/inc/js/std-scripts.js"></script>
<!--[if lt IE 7]><script language="javascript" type="text/javascript" src="/inc/js/sleight.js"></script><![endif]-->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-5698511-1']);
  _gaq.push(['_setDomainName', 'transdirect.com.au']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<style type="text/css">
<!--
.style3 {font-size: 14px}
.style6 {font-size: 16px; color: #000099; }
.style8 {font-size: 14px; color: #FF9933; }
.style9 {font-size: 16px; color: #000000; }
-->
</style>
<!-- search engine tracking script V.1.0 --> 
<script type="text/javascript">//<![CDATA[ 
var ns_data,ns_hp,ns_tz,ns_rf,ns_sr,ns_img,ns_pageName; 
ns_pageName= this.location; 
document.cookie='__support_check=1';ns_hp='http'; 
ns_rf=document.referrer;ns_sr=window.location.search; 
ns_tz=new Date();if(location.href.substr(0,6).toLowerCase() == 'http:') 
ns_hp='http';ns_data='&an='+escape(navigator.appName)+ 
'&sr='+escape(ns_sr)+'&ck='+document.cookie.length+ 
'&rf='+escape(ns_rf)+'&sl='+escape(navigator.systemLanguage)+ 
'&av='+escape(navigator.appVersion)+'&l='+escape(navigator.language)+ 
'&pf='+escape(navigator.platform)+'&pg='+escape(ns_pageName); 
ns_data=ns_data+'&cd='+screen.colorDepth+'&rs='+escape(screen.width+ ' x '+screen.height)+ 
'&tz='+ns_tz.getTimezoneOffset()+'&je='+ navigator.javaEnabled(); 
ns_img=new Image();ns_img.src=ns_hp+'://tracker.statgauge.com/statistics.aspx'+ 
'?v=1&s=213&acct=225183'+ns_data+'&tks='+ns_tz.getTime(); //]]> 
</script> 
<!-- End search engine tracking script -->  
</head>

<body>
	<div id="header-outer">
  	<div id="header">
    	<h1><a href="/" title=""><span>Post / send / transport / freight / courier your package or parcel from A to B - Ebay specialists!</span></a></h1>   
      <a class="airlink" href="/accountquery/" onclick="_gaq.push(['_trackEvent', 'Account Query', 'Button Click', '/accountquery/']);"></a>
	   </div>
    
    <div id="top-nav-span">
      <div id="top-nav-wrap">
        <ul id="top-nav">
            <li id="home"><a href="/" title=""><span>Home</span></a></li>
            <li id="how-it-works"><a class = "current" href="/how-it-works/" title=""><span>How It Works?</span></a>
				<ul>
					<li class="road"><a href="/how-it-works/road-freight/">Road Freight </a></li>
					<li class="air"><a href="/how-it-works/air-freight/">Air Freight</a></li>
				</ul>
			</li>
            <li id="why-transdirect"><a href="/why-transdirect-couriers/" title=""><span>Why Transdirect Couriers?</span></a></li>
            <li id="insurance"><a href="/parcel-insurance/" title=""><span>Parcel Insurance</span></a>
				<ul>
					<li class="road"><a href="/parcel-insurance/road-freight/">Road Freight </a></li>
					<li class="air"><a href="/parcel-insurance/air-freight/">Air Freight</a></li>
				</ul>
			</li>
            <li id="contact"><a href="/contact/" title=""><span>Contact Us</span></a></li>
        </ul>
      </div>   
    </div>
  </div>
  
<div id="subpage-outer" class="quote">

  	<div id="content">    	
    	<div id="left">
<div class="padFix">
<a name="top"></a>
<h3>Air freight - Frequently asked questions</h3>
<p class="style3">Please visit our <a href="/faq/air-freight/">frequently asked questions</a> page to find answers to common questions users have about our service.  After reading our <a href="/faq/air-freight/">frequently asked questions</a> and this 'how it works' page, you should be familiar with our service &amp; ready to book your consignment!</p>
<p>&nbsp;</p>

<a name="howitworks"></a>
<h3>How it works  - <a href="#top">^</a>  </h3>
<ul style="font-size:14px;">
		<li> <span class="style9">Step 1:</span> Get a quick quote. It's so simple!</li>
		<li> <span class="style9">Step 2:</span> Pay and confirm your details through our secure payment facility.</li>
		<li> <span class="style9">Step 3:</span> You will receive an email with an attachment including a Tax Invoice and Order Confirmation.</li>
		<li> <span class="style9">Step 4:</span> Depending on the time you made the booking, you will receive your consignment label separately at the next available dispatch time. We send consignment note emails at 9am, 1pm and 5pm Monday to Friday. So if you book at 10pm, you will receive the consignment label at 9am the following morning, etc. </li>
		<li> <span class="style9">Step 5:</span> Print out the consignment note and securely stick it to the carton. Make sure you write the delivery address and the consignment number on the carton, just in case the label is accidentally removed. You must have the label secured on the carton for the pickup to be taken.</li>
		<li><span class="style9">Step 6:</span> One of our chosen delivery companies will come and pick up the carton on the day and time specified.</li>
		  <li><span class="style9">Step 7:</span> You can track and trace your delivery status on our web site, using your consignment number.</li>
		  <li><span class="style9">Step 8:</span> Your consignment will be delivered between the hours of 9am - 5pm.</li>
          <p>&nbsp;</p>
</ul>
<a name="requirements"></a>
<h3>Packing &amp; Sending Requirements  - <a href="#top">^</a>  </h3>
<ul style="font-size:14px;">
		<li> Don't make the carton too full or too heavy. If the contents of the carton can be split and it weighs over 30kg, put it in two cartons.</li>
		<li> Make sure your box/carton is structurally sturdy.</li>
		<li> Don't over pack your carton.</li>
		<li> Make sure your goods have cushioning around it so it reduces the chances of getting damaged in transit.</li>

		<li> Make sure the cartons are well sealed with wide robust tape. Secure the carton to eliminate it from opening whilst in transit.</li>
		<li> If your goods are fragile, let us know by clearly writing on the carton <strong>FRAGILE!</strong></li>
		<li> We do not send:<br>
		  - dangerous goods
		  <br>

		   - any unpackaged items<br>
	      - items that exceed 4 metres in length</li>
		<li> Make sure you are aware of the <a target="_blank" href="/terms/">terms and conditions.</a></li>
		<li> Once you have booked and paid for the pickup you will receive an attachment of your receipt and consignment note via email. You need to print out this consignment note and stick it on to your carton.<br>
	    </li>

		<li> Make sure your carton has no other address written on it, or any old delivery labels.</li>
		  <li> Write on the carton, the delivery address and the consignment number.</li>
          <li>All items must be packed  into a carton/box or covered in either bubblewrap or plastic depending on the  nature of the item. If damage can easily occur due to the packaging used, you  may void the transit warranty.</li>
		  <li>Suitcases and travel cases can be sent as they are. They do need to be securely fastened.</li>
		  <li>Large unpackaged furniture  items will not be accepted. These types of items would be best moved with a  specialised furniture removalist. You can contact our Customer service on 1300  668 229 if you need to discuss your delivery. </li>
		  <li>Items that exceed 60kgs must be packed on a skid/pallet or crate, or be packaged in a way that can be moved easily and safely by forklift. You must have a forklift or other lifting method available at both the sending &amp; receiving addresses If you do not have the appropriate lifting equipment, you must select the Hydraulic Tail lift option for that location.</li>
		<li>If your packaged item is between 35 -60 kg's and you do not have a pallet, you must ensure there is sufficient assistance to load and/or unload the vehicle. Failure to have the assistance required will result in either a Futile pickup or Re-delivery fee.</li>
</ul>
<p>&nbsp;</p>
<a name="measuring"></a>
<h3>Measuring the size of the carton -  <a href="#top">^</a> </h3>
<p class="style3">Please review the image below showing how to correctly measure the size of your carton(s):</p>
<p><img src="/img/measure.jpg"></p>
<a name="tracking"></a>
<h3>Tracking your consignment - <a href="#top">^</a> </h3>
<p style="font-size:14px;">If you need to track your consignment at any time, just fill in the consignment number below and press 'Track'.</p>
<form action="./track.php" method="get" name="frmQuickSearch">
                      <table id="startTable">
                          <tbody><tr>
                              <td>Consignment/Article Number:</td>

                              <td>
                                  <input type="text" name="connote">&nbsp; <input type="submit" value="Track">                             </td>
                          </tr>
                      </tbody></table>
            </form>
			<br>
			<a name="regular"></a>
			<h3>Are you a regular sender or receiver? - <a href="#top">^</a> </h3>
			<p class="style3">Although we have structured our business to offer cheap rates across the board, we do offer clients that regularly send with transdirect extra savings by becoming a member. <br><br>
If you send over $500 a week on your current freight requirements we will offer you up to 5% discount.<br><br>
 If you send over $1000 per week we will offer you up to 10% discount. <br><br>
 If you send over $2000 per week we will offer you up to 15% discount.<br><br>
We also have a range of Credit account options available for businesses. <br><br>
The newest inclusion to our services is our API system. This is designed to make the booking process much more efficient for your dispatch/warehouse staff. This system can also integrate our quoting system into your website.<br><br>

To find out further details and whether you qualify for an account, please  <a href="/contact">contact us.</a></p>
            <h3>Questions?</h3>
            <p><span class="style3">If you have any  questions you can contact us at via the methods listed on our <a href="/contact">contact page</a></span></p>
    	    <div class="float-fix"></div>
        </div>
       </div>
       <div id="right">
      	<div class="padFix">
        	
					  <h2 style="color:FF6D0C;">Translinks</h2>
		  <ul>
			<li><a href="/faq/air-freight/" title="">Frequently Asked Questions</a></li>
           	  <li><a href="./#howitworks" title="">How it Works</a></li>
           	 <? // <li><a href="./#compare" title="">Compare our Rates</a></li> ?>
            <li><a href="./#requirements" title="">Packing &amp; Sending Requirements</a></li>
            <li><a href="./#measuring" title="">Measuring Info</a></li>
            <li><a href="./#tracking" title="">Tracking Info</a></li>
            <li><a href="./#times" title="">Transit Times</a></li>
            <li><a href="./#regular" title="">Regular Sender?</a></li>
                      </ul>
        </div>
      </div>
      <div class="float-fix"></div> 
    </div>
  </div>
  
  
  <div id="footer-outer">
  	<div id="footer">
    	<div class="left">
    		<p><strong>TransDirect</strong> - Parcel Courier Service - Door to Door Anywhere in Australia!<br />
        <script type="text/javascript">generateEmailLink('info', 'transdirect.com.au');</script><br />

        ph. 1300 668 229<br />
		<a href="http://www.djc.com.au/services/#myob-system-integration" target="_blank">MYOB Accounting System &amp; Web Integration by DJC</a></p>
      </div>
      <div class="right">
      	<p><a href="/contact" title="">CONTACT</a> | <a href="/terms/" title="">TERMS &amp; CONDITIONS</a></p>
        <p>&copy; 2008-2012 TransDirect - <a href="/resources">Resources</a> <br>
			<a href="http://courier-brisbane.transdirect.com.au/">Brisbane</a> - <a href="http://courier-adelaide.transdirect.com.au/">Adelaide</a> - 
			<a href="http://courier-canberra.transdirect.com.au/">Canberra</a> - <a href="http://courier-darwin.transdirect.com.au/">Darwin</a> - 
			<a href="http://courier-hobart.transdirect.com.au/">Hobart</a> - <a href="http://courier-melbourne.transdirect.com.au/">Melbourne</a> - 
			<a href="http://courier-perth.transdirect.com.au/">Perth</a> - <a href="http://courier-sydney.transdirect.com.au/">Sydney</a>
		</p>
      </div>
      <div class="float-fix"></div>
    </div>
  </div>
</body>
</html>
