<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>How Transdirect Works</title>

<meta name="keywords" content="Post, send , transport, freight, courier, package, parcel, ebay, shipping" />
<meta name="description" content="Transdirect door to door freight for everybody." />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="en-au" />
<meta name="robots" content="index, follow " />
<link rel="stylesheet" href="/css/screen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/print.css" type="text/css" media="print" />
<link rel="stylesheet" href="/css/sIFR-screen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/sIFR-print.css" type="text/css" media="print" />
<link rel="stylesheet" href="/css/nav.css" type="text/css" media="screen" />
<link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="/inc/js/swfobject.js"></script>
<script type="text/javascript" src="/inc/js/sifr.js"></script>
<script type="text/javascript" src="/inc/js/mootools.v1.11.js"></script>
<script type="text/javascript" src="/inc/js/std-scripts.js"></script>
<!--[if lt IE 7]><script language="javascript" type="text/javascript" src="/inc/js/sleight.js"></script><![endif]-->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-5698511-1']);
  _gaq.push(['_setDomainName', 'transdirect.com.au']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<style type="text/css">
<!--
.style3 {font-size: 14px}
.style6 {font-size: 16px; color: #000099; }
.style8 {font-size: 14px; color: #FF9933; }
.style9 {font-size: 16px; color: #000000; }
-->
</style>
<!-- search engine tracking script V.1.0 --> 
<script type="text/javascript">//<![CDATA[ 
var ns_data,ns_hp,ns_tz,ns_rf,ns_sr,ns_img,ns_pageName; 
ns_pageName= this.location; 
document.cookie='__support_check=1';ns_hp='http'; 
ns_rf=document.referrer;ns_sr=window.location.search; 
ns_tz=new Date();if(location.href.substr(0,6).toLowerCase() == 'http:') 
ns_hp='http';ns_data='&an='+escape(navigator.appName)+ 
'&sr='+escape(ns_sr)+'&ck='+document.cookie.length+ 
'&rf='+escape(ns_rf)+'&sl='+escape(navigator.systemLanguage)+ 
'&av='+escape(navigator.appVersion)+'&l='+escape(navigator.language)+ 
'&pf='+escape(navigator.platform)+'&pg='+escape(ns_pageName); 
ns_data=ns_data+'&cd='+screen.colorDepth+'&rs='+escape(screen.width+ ' x '+screen.height)+ 
'&tz='+ns_tz.getTimezoneOffset()+'&je='+ navigator.javaEnabled(); 
ns_img=new Image();ns_img.src=ns_hp+'://tracker.statgauge.com/statistics.aspx'+ 
'?v=1&s=213&acct=225183'+ns_data+'&tks='+ns_tz.getTime(); //]]> 
</script> 
<!-- End search engine tracking script -->  


<script>
function showHide(valCk){
	
	if(valCk=='air'){
		window.location.href = '/how-it-works/air-freight/'+window.location.hash;
		document.getElementById("airLink").setAttribute("href",'/how-it-works/air-freight/'+window.location.hash);
	 }
	 else{
		window.location.href = '/how-it-works/road-freight/'+window.location.hash;
		document.getElementById("roadLink").setAttribute("href",'/how-it-works/road-freight/'+window.location.hash);
	 }
}

</script>

</head>

<body>
	<div id="header-outer">
  	<div id="header">
    	<h1><a href="/" title=""><span>Post / send / transport / freight / courier your package or parcel from A to B - Ebay specialists!</span></a></h1>   
      <a class="airlink" href="/accountquery/" onclick="_gaq.push(['_trackEvent', 'Account Query', 'Button Click', '/accountquery/']);"></a>
	  </div>
    
    <div id="top-nav-span">
      <div id="top-nav-wrap">
        <ul id="top-nav">
            <li id="home"><a href="/" title=""><span>Home</span></a></li>
            <li id="how-it-works"><a class = "current" href="/how-it-works/" title=""><span>How It Works?</span></a>
				<ul>
					<li class="road"><a href="/how-it-works/road-freight/">Road Freight </a></li>
					<li class="air"><a href="/how-it-works/air-freight/">Air Freight</a></li>
				</ul>
			</li>
            <li id="why-transdirect"><a href="/why-transdirect-couriers/" title=""><span>Why Transdirect Couriers?</span></a></li>
            <li id="insurance"><a href="/parcel-insurance/" title=""><span>Parcel Insurance</span></a>
				<ul>
					<li class="road"><a href="/parcel-insurance/road-freight/">Road Freight </a></li>
					<li class="air"><a href="/parcel-insurance/air-freight/">Air Freight</a></li>
				</ul>
			</li>
            <li id="contact"><a href="/contact/" title=""><span>Contact Us</span></a></li>
        </ul>
      </div>   
    </div>
  </div>
  
<div id="subpage-outer" class="quote">

  	<div id="content">
    	<div id="left">
		<div class="check">
		<div class="boxes" onclick="showHide('road');">
			<!-- <div class="icon"><img src="/icon-truck.png" alt="" /></div> -->
			<div class="box-info">
				<h6>Road Freight Services</h6>
				<p>General information for Road Freight</p>
				<a href="" id="roadLink">Click here for details</a>
				<div class="clearfix"></div>
			
			</div>
		</div>

		<div class="boxes" onclick="showHide('air');">
			<!-- <div class="icon"><img src="/icon-plane.png" alt="" /></div> -->
			<div class="box-info">
				<h6>Air Freight Services</h6>
				<p>General information for Air Freight</p>
				<a href="" id="airLink">Click here for details</a>
				<div class="clearfix"></div>
			
			</div>
		</div>
	</div>

        </div>
       <div id="right">
      	<div class="padFix">
        	
					  <h2 style="color:FF6D0C;">Translinks</h2>
		  <ul>
			<li><a href="/faq" title="">Frequently Asked Questions</a></li>
           	  <li><a href="./#howitworks" title="">How it Works</a></li>
           	 <? // <li><a href="./#compare" title="">Compare our Rates</a></li> ?>
            <li><a href="./#requirements" title="">Packing &amp; Sending Requirements</a></li>
            <li><a href="./#measuring" title="">Measuring Info</a></li>
            <li><a href="./#tracking" title="">Tracking Info</a></li>
            <li><a href="./#times" title="">Transit Times</a></li>
            <li><a href="./#regular" title="">Regular Sender?</a></li>
                      </ul>
        </div>
      </div>
      <div class="float-fix"></div> 
    </div>
  </div>
  
  
  <div id="footer-outer">
  	<div id="footer">
    	<div class="left">
    		<p><strong>TransDirect</strong> - Parcel Courier Service - Door to Door Anywhere in Australia!<br />
        <script type="text/javascript">generateEmailLink('info', 'transdirect.com.au');</script><br />

        ph. 1300 668 229<br />
		<a href="http://www.djc.com.au/services/#myob-system-integration" target="_blank">MYOB Accounting System &amp; Web Integration by DJC</a></p>
      </div>
      <div class="right">
      	<p><a href="/contact" title="">CONTACT</a> | <a href="/terms/" title="">TERMS &amp; CONDITIONS</a></p>
        <p>&copy; 2008-2012 TransDirect - <a href="/resources">Resources</a> <br>
			<a href="http://courier-brisbane.transdirect.com.au/">Brisbane</a> - <a href="http://courier-adelaide.transdirect.com.au/">Adelaide</a> - 
			<a href="http://courier-canberra.transdirect.com.au/">Canberra</a> - <a href="http://courier-darwin.transdirect.com.au/">Darwin</a> - 
			<a href="http://courier-hobart.transdirect.com.au/">Hobart</a> - <a href="http://courier-melbourne.transdirect.com.au/">Melbourne</a> - 
			<a href="http://courier-perth.transdirect.com.au/">Perth</a> - <a href="http://courier-sydney.transdirect.com.au/">Sydney</a>
		</p>
      </div>
      <div class="float-fix"></div>
    </div>
  </div>
</body>
</html>
